import argparse
import hashlib
from collections import namedtuple
from typing import List

Result = namedtuple('Result', ['data', 'number'])


def get_hash(data: bytes, max_number: int, seed: int = 0):
    data += str(seed).encode()
    data_hash = hashlib.sha512(data)
    data_hex = data_hash.hexdigest()
    data_int = int(data_hex, 16)
    return data_int % max_number + 1


def distribute(data: List[str], max_number: int, seed: int = 0) -> List[Result]:
    result = []
    for line in data:
        result.append(Result(line, get_hash(line.encode(), max_number, seed)))
    return result


def main(file: str, max_number: int, seed: int = 0):
    with open(file, 'r') as f:
        data = f.read().splitlines()
    max_number = max_number or len(data)
    new_lines = distribute(data, max_number, seed)
    for line in new_lines:
        print(line.data, line.number)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Distribute unique numbers for strings in file')
    parser.add_argument(
        '--max_number', type=int, default=None,
        help='max number to distribute (default: rows count)',
    )
    parser.add_argument(
        '--seed', type=int, default=0,
        help='seed for random distribution',
    )
    parser.add_argument(
        '--file', type=str, required=True,
        help='file with data',
    )
    args = parser.parse_args()
    main(args.file, args.max_number, args.seed)
