# Distributor

This program distributes numbers evenly and
deterministically between the lines in the file.

# Run

1. You should install python version 3.7 or higher.
2. You can run the script with the following command:
```shell script
python3 main.py --file <path_to_file> --max_number 20 --seed 42
```
Arguments:
- `file` - path to file with data, required.
- `max_number` - maximum number of random (e.g. number of exam 
questions). Number of lines in file will be used by default.
- `seed` - number that change random distribution. Zero by default.
